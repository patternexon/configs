" Specify a directory for plugins

call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'scrooloose/syntastic'
Plug 'bling/vim-airline'
Plug 'flazz/vim-colorschemes'
Plug 'fatih/vim-go' " golang specific
call plug#end()

"There is something wrong with vim-go plugin
let g:go_version_warning = 0
""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""
"Basic stuff for prettying up
""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""

"load ftplugins and indent files
filetype plugin on
filetype indent on

"allow backspacing over everything in insert mode
set backspace=indent,eol,start

"store lots of :cmdline history
set history=10000

set showcmd "show incomplete cmds down the bottom
set showmode "show current mode down the bottom
set showmatch
set incsearch "find the next match as we type the search
set hlsearch "hilight searches by default

set cursorline
set nowrap "dont wrap lines
set linebreak "wrap lines at convenient points

"not using arrow keys anymore

"indent settings
set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent

"folding settings
set foldmethod=indent "fold based on indent
set foldnestmax=3 "deepest fold is 3 levels
set nofoldenable "dont fold by default

set wildmode=list:longest "make cmdline tab completion similar to bash
set wildmenu "enable ctrl-n and ctrl-p to scroll thru matches
set wildignore=*.o,*.obj,*~ "stuff to ignore when tab completing

"display tabs and trailing spaces
set list
set listchars=tab:▷⋅,trail:⋅,nbsp:⋅

set formatoptions-=o "dont continue comments when pushing o/O

"vertical/horizontal scroll off settings
set scrolloff=3
set sidescrolloff=7
set sidescroll=1

"Better Search
set ignorecase
set smartcase
set incsearch

"turn on syntax highlighting
syntax on
"
""some stuff to get the mouse going in term
set mouse=a
set ttymouse=xterm2
"
"tell the term has 256 colors
set t_Co=256
"
"hide buffers when not displayed
set hidden
"
"show number lines
set nu
"
"" colorscheme
colorscheme molokai

let mapleader = ","


""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""
" Plugin specifc mapping
"""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""
" NERDTree
let NERDTreeChDirMode=2
" starts when no file is given
autocmd vimenter * if !argc() | NERDTree | endif
" Ctrl+n
map <C-n> :NERDTreeToggle<CR>


